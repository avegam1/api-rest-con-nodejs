create  database vehiculos;
use vehiculos;

create table Autos(
	id int (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(180) NOT NULL,
	valor INT(100)NOT NULL,
	fecha_ingreso  date NOT NULL,
	modelo int (100) NOT NULL,
	img VARCHAR(200),
	creat_at timestamp default current_timestamp
);
describe Autos;
