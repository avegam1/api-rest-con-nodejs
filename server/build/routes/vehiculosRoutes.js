"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const vehiculosControllers_1 = __importDefault(require("../controllers/vehiculosControllers"));
class VehiculosRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', vehiculosControllers_1.default.list);
        this.router.get('/:id', vehiculosControllers_1.default.getOne);
        this.router.post('/', vehiculosControllers_1.default.create);
        this.router.put('/:id', vehiculosControllers_1.default.update);
        this.router.delete('/:id', vehiculosControllers_1.default.delete);
    }
}
const vehiculosroutes = new VehiculosRoutes();
exports.default = vehiculosroutes.router;
