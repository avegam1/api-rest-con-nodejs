"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const inexControllers_1 = require("../controllers/inexControllers");
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', inexControllers_1.indexControllers.index);
    }
}
const indexroutes = new IndexRoutes();
exports.default = indexroutes.router;
