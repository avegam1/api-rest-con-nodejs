import {Request, Response}  from 'express';
import pool from '../database';
class VehiculosControllers{

		public async list (req :Request, res:Response): Promise<void> {
		
			const vehiculos = await pool.query('SELECT * FROM autos')
			res.json(vehiculos);
	}

		public async getOne (req :Request, res:Response): Promise<any> {
			const {id}=req.params;
			const vehiculo =await pool.query('SELECT * FROM autos WHERE id=  ?', [id]);
			if(vehiculo.length>0){
				return res.json(vehiculo[0]);
		}
		res.status(404).json({text: 'el auto no existe'});
		
		
	}

		public async create(req:Request, res:Response): Promise<void> {
			await pool.query('INSERT INTO autos set ?', [req.body]);
			res.json({message:'Auto Creado'});
	}

		public async update(req:Request, res:Response):Promise<void>{
			const {id}=req.params;
			await pool.query('UPDATE  autos set ? WHERE id= ? ', [req.body, id]);
			res.json({text:'automovil actualizado '});
	}

		public async delete(req:Request, res:Response): Promise<void>{
			const {id}=req.params;
			await pool.query('DELETE  FROM autos WHERE id=  ?', [id]);
			res.json({text:'el juego fue eliminado'});
	}
}
 const vehiculoControllers= new VehiculosControllers();
 export default vehiculoControllers;
