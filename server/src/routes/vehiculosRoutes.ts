import {Router} from 'express';
import vehiculoControllers from '../controllers/vehiculosControllers'

class VehiculosRoutes{
	public router: Router=Router();
	constructor(){
		this.config();
	}
	config(): void{
		this.router.get('/', vehiculoControllers.list);
		this.router.get('/:id', vehiculoControllers.getOne);
		this.router.post('/', vehiculoControllers.create );
		this.router.put('/:id', vehiculoControllers.update );
		this.router.delete('/:id', vehiculoControllers.delete );

	}
}
const vehiculosroutes= new VehiculosRoutes();
export default vehiculosroutes.router;