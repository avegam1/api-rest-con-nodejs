import {Router} from 'express';
import {indexControllers} from'../controllers/inexControllers';

class IndexRoutes{
	public router: Router=Router();
	constructor(){
		this.config();
	}
	config(): void{
		this.router.get('/', indexControllers.index );

	}
}
const indexroutes= new IndexRoutes();
export default indexroutes.router;